import {FC, ReactElement, useEffect, useState, useContext} from 'react';
import axios from 'axios';

import Loader from '../components/Loader';
import ArticlePage from '../components/ArticlePage';

import {ThemeContext} from '../context/Themes';

import '../css/Articles.css';

const Article: FC = (): ReactElement => {
  const [isLoading, setIsLoading] = useState<true | false>(true);
  const [articlesData, setArticlesData] = useState<any>();
  const [isArticleSelected, setIsArticleSelected] = useState<true | false>(false);
  const [articleName, setArticleName] = useState<string>('');

  const {theme} = useContext(ThemeContext);

  useEffect(() => {
    axios
      .get<string>('https://maskedman99.gitlab.io/articles/index.json')
      .then(response => {
        setArticlesData(response.data);
        setIsLoading(false);
      })
      .catch(error => alert(error));
  }, []);

  return isLoading ? (
    <Loader />
  ) : (
    <div className="Articles-container">
      {isArticleSelected ? (
        <ArticlePage articleURL={`https://maskedman99.gitlab.io/articles/page/${articleName}.html`} />
      ) : (
        articlesData.map((item: ArticleData) => (
          <div
            key={item.id}
            className="Articles-title"
            style={{color: theme.foreground}}
            onClick={() => {
              setArticleName(`_${item.id}_${item.title.replaceAll(' ', '_')}`);
              setIsArticleSelected(true);
            }}>
            {item.title}
          </div>
        ))
      )}
    </div>
  );
};

type ArticleData = {
  id: number;
  title: string;
};

export default Article;
