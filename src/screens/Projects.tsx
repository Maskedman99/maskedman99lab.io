import {FC, ReactElement} from 'react';

import ProjectGrid from '../components/ProjectGrid';

import Project1 from '../assets/images/libgen.png';
import Project2 from '../assets/images/mec.png';
import Project3 from '../assets/images/covidstats.png';

import '../css/Projects.css';

const Projects: FC = (): ReactElement => {
  return (
    <div className="Projects-main">
      <div className="grid-container">
        <ProjectGrid
          image={Project1}
          title="LibGen"
          desc="App to download books and papers from Library Genesis on the go."
          madeUsing="React-native"
          srcLink="https://github.com/Maskedman99/LibGen"
        />
        <ProjectGrid
          image={Project2}
          title="MEC"
          desc="App to find the attendance of students in my college by scrapping data from the college website."
          madeUsing="React-native"
          srcLink="https://github.com/Maskedman99/MEC"
          link="https://play.google.com/store/apps/details?id=com.maskedmanmec"
        />
        <ProjectGrid
          image={Project3}
          title="covidstats"
          desc="App to find statistics on covid cases"
          madeUsing="React-native"
          srcLink="https://github.com/Maskedman99/covidstats"
        />
      </div>
    </div>
  );
};

export default Projects;
