import {useState, FC, ReactElement} from 'react';

import Skills from '../components/Skills';
import CertificationCard from '../components/CertificationCard';

import ibs from '../assets/images/ibs.png';
import avasarshala from '../assets/images/avasarshala.png';
import coursera from '../assets/images/coursera.png';
import hackerrank from '../assets/images/hackerrank.png';
import stationx from '../assets/images/stationx.png';
import google from '../assets/images/google.png';
import vidyodaya from '../assets/images/vidyodaya.png';
import model from '../assets/images/model.png';
import uc from '../assets/images/uc.png';

import '../css/Resume.css';

const Resume: FC = (): ReactElement => {
  const [open, setOpen] = useState('Skills');
  return (
    <div className="Resume-Container">
      <div
        className={open === 'Skills' ? 'Resume-ActiveTitle' : 'Resume-Title'}
        onClick={() => setOpen('Skills')}>
        Skills
      </div>
      {open === 'Skills' && (
        <Skills
          skills={[
            'Python',
            'R',
            'ReactJS',
            'React-native',
            'HTML',
            'CSS',
            'Java',
            'Bash',
            'Javascript',
            'Git'
          ]}
        />
      )}
      <div
        className={open === 'Experience' ? 'Resume-ActiveTitle' : 'Resume-Title'}
        onClick={() => setOpen('Experience')}>
        Experience
      </div>
      {open === 'Experience' && (
        <>
          <CertificationCard
            image={ibs}
            title={'IBS Software'}
            subtitle1={'Software Engineer'}
            subtitle2={
              'Built APIs for airlines that are aligned to the NDC structure, focused on Offer management and Order management.'
            }
          />
          <CertificationCard
            image={avasarshala}
            title={'Avasarshala'}
            subtitle1={'Intern'}
            subtitle2={
              'Developed a website that helps administrator to view and manage the platform and help analyze user data.'
            }
            link={
              'https://gitlab.com/Maskedman99/maskedman99.gitlab.io/raw/dev/.certificates/Avasarshala.pdf'
            }
          />
        </>
      )}
      <div
        className={open === 'Education' ? 'Resume-ActiveTitle' : 'Resume-Title'}
        onClick={() => setOpen('Education')}>
        Education
      </div>
      {open === 'Education' && (
        <>
          <CertificationCard
            image={uc}
            title={'University of Canterbury'}
            subtitle1={'Christchurch, New Zealand'}
            subtitle2={'Master in Applied Data Science'}
          />
          <CertificationCard
            image={model}
            title={'Govt. Model Engineering College'}
            subtitle1={'Thrikkakara, Kerala, India'}
            subtitle2={'B.Tech in Computer Science & Engineering'}
          />
          <CertificationCard
            image={vidyodaya}
            title={'Vidyodaya School'}
            subtitle1={'Thevakkal, Kerala, India'}
            subtitle2={'Higher Secondary Education'}
          />
        </>
      )}
      <div
        className={open === 'Certifications' ? 'Resume-ActiveTitle' : 'Resume-Title'}
        onClick={() => setOpen('Certifications')}>
        Certifications
      </div>
      {open === 'Certifications' && (
        <>
          <CertificationCard
            image={coursera}
            title={'Introduction to Big Data'}
            subtitle1={'UC San Diego'}
            subtitle2={'Coursera'}
            link={'https://www.coursera.org/account/accomplishments/certificate/SEQWRPDWRL4C'}
          />
          <CertificationCard
            image={coursera}
            title={'R Programming'}
            subtitle1={'John Hopkins University'}
            subtitle2={'Coursera'}
            link={'https://www.coursera.org/account/accomplishments/certificate/9TVNB8S6YNFP'}
          />
          <CertificationCard
            image={hackerrank}
            title={'React Certification'}
            subtitle1={'HackerRank'}
            link={'https://www.hackerrank.com/certificates/6f8c98030869'}
          />
          <CertificationCard
            image={google}
            title={'Google Analytics for Beginners'}
            subtitle1={'Google Analytics Academy'}
            link={
              'https://gitlab.com/Maskedman99/maskedman99.gitlab.io/-/raw/dev/.certificates/Google_Analytics_Begineer.pdf'
            }
          />
          <CertificationCard
            image={stationx}
            title={'The Complete Cyber Security Course! Volume 1: Hackers Exposed'}
            subtitle1={'Nathan House'}
            subtitle2={'StationX'}
            link={
              'https://gitlab.com/Maskedman99/maskedman99.gitlab.io/-/raw/dev/.certificates/StationX%20volume-1.pdf'
            }
          />
        </>
      )}
    </div>
  );
};

export default Resume;
