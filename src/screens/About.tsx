import {FC, ReactElement} from 'react';

import AboutUrls from '../components/AboutUrls';

import Pic from '../assets/images/profilePic.jpeg';
import AltPic from '../assets/images/infinity.png';

import '../css/About.css';

const About: FC = (): ReactElement => {
  return (
    <div className="About-main">
      <div className="About-imageFlipper">
        <img src={AltPic} alt="" width="100" height="100" className="About-ProfilePic" />
        <img src={Pic} alt="" width="100" height="100" className="About-ProfilePic" />
      </div>
      <h1 className="About-title">About me!</h1>
      <div className="About-text">
        I am Awsome!! Just like you... Testing Testing Testing :) blah blah blah blah blah blah blah blah bla
        cds cdskmcke cdnskaufhomejruamcboxirwmct
      </div>
      <AboutUrls orientation="horizontal" />
    </div>
  );
};

export default About;
