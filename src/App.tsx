import {useContext, useState, lazy, Suspense, FC, ReactElement} from 'react';
import {Route, Routes} from 'react-router-dom';
import {MemoryRouter} from 'react-router-dom';

import {themes, ThemeContext} from './context/Themes';

import './css/App.css';

import Loader from './components/Loader';
import Navbar from './components/Navbar';

const Main = lazy(() => import('./screens/Main'));
const About = lazy(() => import('./screens/About'));
const Projects = lazy(() => import('./screens/Projects'));
const Articles = lazy(() => import('./screens/Articles'));
const Resume = lazy(() => import('./screens/Resume'));

const App: FC = (): ReactElement => {
  const {theme} = useContext(ThemeContext);
  const [Theme, setTheme] = useState(theme);

  const changeTheme = async () => {
    Theme.isDark ? setTheme(themes.light) : setTheme(themes.dark);
  };

  return (
    <MemoryRouter>
      <ThemeContext.Provider value={{theme: Theme, changeTheme: changeTheme}}>
        <div className="App" style={{backgroundColor: Theme.background, color: Theme.foreground}}>
          <Navbar />
          <Suspense fallback={<Loader />}>
            <Routes>
              <Route path="/" element={<Main />} />
              <Route path="about" element={<About />} />
              <Route path="projects" element={<Projects />} />
              <Route path="resume" element={<Resume />} />
              <Route path="articles" element={<Articles />} />
            </Routes>
          </Suspense>
        </div>
      </ThemeContext.Provider>
    </MemoryRouter>
  );
};

export default App;
