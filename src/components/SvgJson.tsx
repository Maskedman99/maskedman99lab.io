import React, {FC, ReactElement, useContext} from 'react';

import {ThemeContext} from '../context/Themes';

const SvgJson: FC<SvgJsonProp> = ({icon, iconStyle, className, onClick}): ReactElement => {
  const {theme} = useContext(ThemeContext);

  if (iconStyle === undefined) {
    iconStyle = {fill: theme.foreground};
  } else if (iconStyle.fill === undefined) {
    iconStyle.fill = theme.foreground;
  }

  return (
    <svg className={className} viewBox={icon.viewBox} style={iconStyle} onClick={onClick}>
      <path d={icon.path} />
    </svg>
  );
};

type SvgJsonProp = {
  className?: string;
  icon: {
    path: string;
    viewBox: string;
  };
  iconStyle?: {
    fill?: string;
    width?: string;
    height?: string;
  };
  onClick?: (event: React.MouseEvent<SVGElement, MouseEvent>) => void;
};

export default SvgJson;
