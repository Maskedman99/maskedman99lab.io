import {useContext, FC, ReactElement} from 'react';

import {ThemeContext} from '../context/Themes';

import SvgJson from './SvgJson';

import gitlab from '../assets/svgjson/gitlab.json';
import github from '../assets/svgjson/github.json';
import linkedin from '../assets/svgjson/linkedin.json';
import mail from '../assets/svgjson/mail.json';
import codepen from '../assets/svgjson/codepen.json';

import '../css/AboutUrls.css';

const AboutUrls: FC<{orientation: String}> = ({orientation}): ReactElement => {
  const {theme} = useContext(ThemeContext);

  const imgStyle = {fill: theme.foreground};

  return (
    <div className={orientation === 'vertical' ? 'About-footer-vertical' : 'About-footer-horizontal'}>
      <a
        className="About-links"
        target="_blank"
        rel="noopener noreferrer"
        href="https://gitlab.com/Maskedman99">
        <SvgJson icon={gitlab} iconStyle={imgStyle} className="About-image" />
      </a>
      <a
        className="About-links"
        target="_blank"
        rel="noopener noreferrer"
        href="https://github.com/Maskedman99">
        <SvgJson icon={github} iconStyle={imgStyle} className="About-image" />
      </a>
      <a
        className="About-links"
        target="_blank"
        rel="noopener noreferrer"
        href="https://www.linkedin.com/in/rohit-prasad-819a8015a/">
        <SvgJson icon={linkedin} iconStyle={imgStyle} className="About-image" />
      </a>
      <a
        className="About-links"
        target="_blank"
        rel="noopener noreferrer"
        href="https://codepen.io/Maskedman99/pens/">
        <SvgJson icon={codepen} iconStyle={imgStyle} className="About-image" />
      </a>
      <a className="About-links" target="_blank" rel="noopener noreferrer" href="mailto:rohitmpaul@gmail.com">
        <SvgJson icon={mail} iconStyle={imgStyle} className="About-image" />
      </a>
    </div>
  );
};

export default AboutUrls;
