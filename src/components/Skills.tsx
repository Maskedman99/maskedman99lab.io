import {FC, ReactElement} from 'react';

import '../css/Skills.css';

const Skills: FC<{skills: string[]}> = ({skills}): ReactElement => {
  return (
    <div className="Skills-Container">
      {skills.map(item => (
        <div key={item} className="Skills-Skill">
          {item}
        </div>
      ))}
    </div>
  );
};

export default Skills;
