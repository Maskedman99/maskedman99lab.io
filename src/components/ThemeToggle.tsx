import {useContext, FC, ReactElement} from 'react';

import {ThemeContext} from '../context/Themes';

import SvgJson from './SvgJson';

import moon from '../assets/svgjson/moon.json';
import sun from '../assets/svgjson/sun.json';

const ThemeToggle: FC = (): ReactElement => {
  const {theme, changeTheme} = useContext(ThemeContext);

  return (
    <div
      style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}
      onClick={() => changeTheme()}>
      {theme.isDark ? (
        <SvgJson icon={sun} iconStyle={{width: '24px', height: '24px', fill: 'orange'}} />
      ) : (
        <SvgJson icon={moon} iconStyle={{width: '24px', height: '24px', fill: '#999999'}} />
      )}
    </div>
  );
};

export default ThemeToggle;
