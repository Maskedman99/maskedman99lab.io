import {useEffect, useState, useContext, FC, ReactElement} from 'react';
import axios from 'axios';
import DOMPurify from 'dompurify';

import {ThemeContext} from '../context/Themes';

import Loader from './Loader';

import '../css/ArticlePage.css';

const ArticlePage: FC<{articleURL: string}> = ({articleURL}): ReactElement => {
  const [data, setData] = useState<string>('');
  const [loading, setLoading] = useState<true | false>(true);

  const {theme} = useContext(ThemeContext);

  useEffect(() => {
    axios
      .get<string>(articleURL)
      .then(response => {
        setData(response.data);
        setLoading(false);
      })
      .catch(error => alert(error));
  }, [articleURL]);

  return (
    <div className="ArticlePage-container" style={{border: `1px solid ${theme.divider}`}}>
      {loading ? (
        <Loader />
      ) : (
        <div className="ArticlePage-content" dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(data)}} />
      )}
    </div>
  );
};

export default ArticlePage;
