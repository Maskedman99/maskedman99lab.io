import {useContext, useState, FC, ReactElement} from 'react';
import {NavLink} from 'react-router-dom';

import {ThemeContext} from '../context/Themes';

import ThemeToggle from './ThemeToggle';
import SvgJson from './SvgJson';

import useWindowDimensions from '../hooks/useWindowDimensions';

import menuIcon from '../assets/svgjson/menu.json';
import closeIcon from '../assets/svgjson/close.json';

import '../css/Navbar.css';

const Navbar: FC = (): ReactElement => {
  const [isNavbarOpen, setIsNavbarOpen] = useState<true | false>(false);
  const {theme} = useContext(ThemeContext);

  const {width} = useWindowDimensions();
  const link = {color: theme.foreground};
  const imgStyle = {fill: theme.foreground};

  const toggleNavbar = () => {
    setIsNavbarOpen(!isNavbarOpen);
  };

  return (
    <div className="Navbar-container">
      <NavLink
        to="/"
        className="Title"
        style={({isActive}) => ({pointerEvents: isActive ? 'none' : 'auto', color: theme.foreground})}>
        ROHIT PRASAD
      </NavLink>
      {width < 800 &&
        (isNavbarOpen ? (
          <SvgJson icon={closeIcon} iconStyle={imgStyle} className="toggle-button" onClick={toggleNavbar} />
        ) : (
          <SvgJson icon={menuIcon} iconStyle={imgStyle} className="toggle-button" onClick={toggleNavbar} />
        ))}
      <div
        className={width > 800 ? 'Navbar-linksHorizontal' : isNavbarOpen ? 'Navbar-linksVertical' : 'None'}>
        <NavLink
          to="/about"
          className={({isActive}) => (isActive ? 'activeLink' : 'Links')}
          style={link}
          onClick={() => setIsNavbarOpen(false)}>
          ABOUT
        </NavLink>
        <NavLink
          to="/projects"
          className={({isActive}) => (isActive ? 'activeLink' : 'Links')}
          style={link}
          onClick={() => setIsNavbarOpen(false)}>
          PROJECTS
        </NavLink>
        <NavLink
          to="/resume"
          className={({isActive}) => (isActive ? 'activeLink' : 'Links')}
          style={link}
          onClick={() => setIsNavbarOpen(false)}>
          RESUME
        </NavLink>
        <NavLink
          to="/articles"
          className={({isActive}) => (isActive ? 'activeLink' : 'Links')}
          style={link}
          onClick={() => setIsNavbarOpen(false)}>
          ARTICLES
        </NavLink>
        <ThemeToggle />
      </div>
    </div>
  );
};

export default Navbar;
