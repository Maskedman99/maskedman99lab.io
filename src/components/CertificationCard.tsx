import {useContext, FC, ReactElement} from 'react';

import {ThemeContext} from '../context/Themes';

import '../css/CertificationCard.css';

const CertificationCard: FC<CertificationCardProp> = ({
  image,
  title,
  subtitle1,
  subtitle2,
  link
}): ReactElement => {
  const {theme} = useContext(ThemeContext);
  return (
    <a
      className="CertificationCard-Container"
      target="_blank"
      rel="noopener noreferrer"
      href={link || undefined}
      style={{color: theme.foreground}}>
      <img className="CertificationCard-Image" src={image} alt="" height="100" width="100" />
      <div>
        <div className="CertificationCard-Title">{title}</div>
        <div className="CertificationCard-Subtitle">{subtitle1}</div>
        {subtitle2 !== undefined && <div className="CertificationCard-Subtitle">{subtitle2}</div>}
      </div>
    </a>
  );
};

type CertificationCardProp = {
  image: any;
  title: string;
  subtitle1: string;
  subtitle2?: string;
  link?: string;
};

export default CertificationCard;
