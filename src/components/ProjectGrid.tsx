import {FC, ReactElement} from 'react';

import '../css/ProjectGrid.css';

const ProjectGrid: FC<ProjectGridProp> = (projectGridProp): ReactElement => {
  return (
    <div className="ProjectGrid-main">
      <div className="ProjectGrid-Image">
        <img src={projectGridProp.image} alt="" width="30%" height="45%" />
      </div>
      <div className="ProjectGrid-details">
        <h1>{projectGridProp.title}</h1>
        <p className="ProjectGrid-para">{projectGridProp.desc}</p> <br />
        <p className="ProjectGrid-para">Made Using: {projectGridProp.madeUsing}</p> <br />
        <a className="ProjectGrid-link" href={projectGridProp.srcLink}>
          Source Code
        </a>
        <br />
        {projectGridProp.link !== undefined && (
          <a href={projectGridProp.link} className="ProjectGrid-link">
            Live Link
          </a>
        )}
      </div>
    </div>
  );
};

type ProjectGridProp = {
  image: any;
  title: string;
  desc: string;
  madeUsing: string;
  srcLink?: string;
  link?: string;
};

export default ProjectGrid;
